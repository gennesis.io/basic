<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @file: init.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 19/03/16 09:08
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | Bootstrap file
 |
 */

if (!function_exists('asset')) {

    /**
     * @param $uri
     * @param bool $print
     * @return string
     */
    function asset($uri, $print = true)
    {
        return url('static/' . $uri, $print);
    }
}

if (!function_exists('download')) {

    /**
     * @param $uri
     * @param string $name
     * @param bool $print
     * @return string
     */
    function download($uri, $name, $print = true)
    {
        if ($name) {
            $uri = $uri . '?name=' . strtoupper(clear(str_replace([' ', '[', ']' , '(', ')'], '_', $name)));
        }
        return url('static/download/' . $uri, $print);
    }
}
