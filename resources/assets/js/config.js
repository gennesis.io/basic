/**
 *
 */
App
    .config(function ($routeProvider) {

        $routeProvider

            .when('/home', {
                templateUrl: function () {
                    return App.route('home', null);
                },
                resolve: [
                    function ($q, $location, $timeout) {
                        return App.security().load($q, $location, $timeout);
                    }
                ]
            })

            .when('/auth/login', {
                templateUrl: function () {
                    return App.route('auth/login', null);
                },
                controller: 'LoginController'
            })

            .when('/auth/logout', {
                controller: function ($routeParams, ServiceApi) {

                    ServiceApi.auth.logout(App.token, $routeParams.back);
                },
                template: ' '
            })

            .when('/auth/register', {
                templateUrl: function () {
                    return App.route('auth/register', null);
                },
                controller: 'LoginController'
            })

            .when('/auth/reset', {
                templateUrl: function () {
                    return App.route('auth/reset', null);
                },
                controller: 'LoginController'
            })

            .when('/view/:module/:entity/:data*', {
                templateUrl: function (url) {
                    return App.route('view', url);
                },
                controller: 'ViewController',
                resolve: [
                    function ($q, $location, $timeout) {
                        return App.security().load($q, $location, $timeout);
                    },
                    function (ControllerService) {
                        return ControllerService.resolve();
                    }
                ]
            })

            .otherwise({
                redirectTo: '/auth/login'
            });
    })

    .config(function ($controllerProvider) {

        App.register = $controllerProvider.register;
    })

    .config(function ($cookiesProvider) {

        $cookiesProvider.defaults.path = App.cookies.path;
    })

    .config(function ($httpProvider) {

        $httpProvider.interceptors.push('httpRequestInterceptor');
    })

    .filter("sanitize", ['$sce', function ($sce) {

        return function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        }
    }]);


App
    .factory(
        'httpRequestInterceptor',
        ['$rootScope', function ($rootScope) {
            return {
                request: function ($config) {
                    $rootScope.token = App.token;
                    $rootScope.logged = false;
                    if (App.token) {
                        $config.headers[App.header] = App.token;
                        $rootScope.logged = true;
                    }
                    return $config;
                }
            };
        }]);

App
    .directive('nameController', ['$compile', '$parse', function ($compile, $parse) {
        return {
            restrict: 'A',
            terminal: true,
            priority: 100000,
            link: function (scope, elem) {
                var name = $parse(elem.attr('name-controller'))(scope);
                elem.removeAttr('name-controller');
                elem.attr('ng-controller', name);
                $compile(elem)(scope);
            }
        };
    }]);