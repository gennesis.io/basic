/**
 *
 * @var {Object}
 */
window.App = angular.module(
    'App',
    ['ngAnimate', 'ngResource', 'ngRoute', 'ngCookies', 'angular-loading-bar', 'localytics.directives', 'ui.bootstrap']
);

/**
 *
 * @returns {string}
 */
App.guid = function () {

    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

/**
 *
 * @param prefix
 * @param entropy
 * @returns {string}
 */
App.uniqid = function (prefix, entropy) {

    var pr = prefix || '',
        en = entropy || false,
        result;

    this.seed = function (s, w) {
        s = parseInt(s, 10).toString(16);
        return w < s.length ? s.slice(s.length - w) : (w > s.length) ? new Array(1 + (w - s.length)).join('0') + s : s;
    };

    result = pr + this.seed(parseInt(new Date().getTime() / 1000, 10), 8) + this.seed(Math.floor(Math.random() * 0x75bcd15) + 1, 5);

    if (en) result += (Math.random() * 10).toFixed(8).toString();

    return result;
};

/**
 *
 * @param value
 * @param width
 * @param char
 * @returns {*}
 */
App.pad = function (value, width, char) {
    char = char || '0';
    value = value + '';
    return value.length >= width ? value : new Array(width - value.length + 1).join(char) + value;
};

/**
 *
 * @param a
 * @param b
 * @returns {boolean}
 */
App.equal = function (a, b) {
    return String(a).toString() === String(b).toString();
};

App.history = {
    'path': 'cache'
};

/**
 *
 * @param root
 * @param url
 * @returns {string}
 */
App.route = function (root, url) {

    var path = root;

    if (url) {
        path = root + '/' + url.module + '/' + url.entity + '/' + url.data;
    }

    var cache = App.history[path];

    if (!cache) {
        cache = App.uniqid();
        App.history[path] = cache;
    }

    return 'routes/' + path + '.' + 'html' + '?t=' + cache;
};

/**
 *
 * @param string
 * @param first
 * @returns {*}
 */
App.camelize = function (string, first) {

    first = angular.isUndefined(first) ? true : first;
    return string
        .replace(/(?:^\w|[A-Z]|\b\w)/g, function (letter, index) {
            return first ? letter.toUpperCase() : (index == 0 ? letter.toLowerCase() : letter.toUpperCase());
        })
        .replace(/-/g, '');
};

/**
 *
 * @param string
 * @returns {XML}
 */
App.uncamelize = function (string) {

    return string.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
};

/**
 *
 * @returns {{load: load}}
 */
App.security = function () {
    return {
        load: function ($q, $location, $timeout) {

            var deferred = $q.defer();

            deferred.resolve();

            if (App.token) {

                return deferred.promise;
            } else {

                $timeout(function () {
                    $location.path('/auth/login');
                }, 100);

                return $q.reject("'/auth/login'");
            }
        }
    };
};

/**
 * @param string
 * @returns {string}
 */
App.lang = function (string) {

    var lang = '';
    if (App.language) {
        var
            dots = string.split('.'),
            current = 0,
            total = dots.length,
            reference = null;

        dots.forEach(function (_dot) {

            current++;

            if (!reference) {
                reference = App.language;
            }
            if (reference[_dot]) {
                reference = reference[_dot];
            }

            if (current === total) {
                lang = reference.toString();
            }
        });
    }
    return lang;
};

/**
 *
 * @param message
 * @constructor
 */
function Exception(message) {
    this.name = "Exception";
    this.message = message;
}