App

// =========================================================================
// MALIHU SCROLL
// =========================================================================

//On Custom Class
    .directive('cOverflow', ['scrollService', function (scrollService) {
        return {
            restrict: 'C',
            link: function (scope, element) {

                if (!$('html').hasClass('ismobile')) {
                    scrollService.malihuScroll(element, 'minimal-dark', 'y');
                }
            }
        }
    }])

    // =========================================================================
    // WAVES
    // =========================================================================

    // For .btn classes
    .directive('btn', function () {
        return {
            restrict: 'C',
            link: function (scope, element) {
                if (element.hasClass('btn-icon') || element.hasClass('btn-float')) {
                    Waves.attach(element, ['waves-circle']);
                }

                else if (element.hasClass('btn-light')) {
                    Waves.attach(element, ['waves-light']);
                }

                else {
                    Waves.attach(element);
                }

                Waves.init();
            }
        }
    })

    // =========================================================================
    // FORM
    // =========================================================================

    .directive('formField', function ($compile) {

        return {
            replace: false
            , restrict: 'E'
            , scope: {
                form: '=',
                items: '=',
                field: '=',
                record: '=',
                listener: '='
            }
            /**
             * @param $scope
             * @param $timeout
             * @param ServiceApi
             */
            , controller: ['$scope', '$timeout', 'ServiceApi', 'ServiceInput', 'ServiceOutput', 'ServiceModel',
                function ($scope, $timeout, ServiceApi, ServiceInput, ServiceOutput, ServiceModel) {

                    $scope.list = {
                        /**
                         *
                         * @param name
                         */
                        add: function (name) {

                            if (!angular.isArray($scope.record[name])) {
                                $scope.record[name] = [];
                            }
                            $scope.record[name].push({id: '', text: ''});
                        },
                        /**
                         *
                         * @param name
                         * @param option
                         */
                        remove: function (name, option) {

                            if (angular.isArray($scope.record[name])) {
                                $scope.record[name].splice($scope.record[name].indexOf(option), 1);
                            }
                        }
                    };

                    $scope.mask = function ($event, record, key, name) {

                        var patterns = {
                                cpf: '###.###.###-##',
                                cnpj: '##.###.###/####-##',
                                telephone: ['(##)####-####', '(##)#-####-####', '####-######'],
                                cep: '#####-###'
                            },
                            pattern = patterns[name];

                        function replaceAll(string, find, replace) {
                            function escapeRegExp(expression) {
                                return expression.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
                            }

                            return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
                        }

                        var
                            data = record[key],
                            masked = '';

                        if (data) {

                            for (var i = 0; i < pattern.length; i++) {
                                data = replaceAll(data, pattern.charAt(i), '');
                            }

                            var
                                k = 0,
                                mask = pattern.split(''),
                                value = data.split('');

                            for (var j = 0; j < mask.length; j++) {
                                if (!value[k]) {
                                    break;
                                }
                                if (mask[j] === '#') {
                                    if (value[k]) {
                                        masked += value[k++];
                                    }
                                } else {
                                    if (mask[j]) {
                                        masked += mask[j];
                                    }
                                }
                            }
                        }

                        record[key] = masked;
                    };

                    $scope.file = function (input, scope) {
                        ServiceInput.file(scope.record, ServiceModel.module, ServiceModel.entity, scope.field.key, input.files);
                    };

                    $scope.output = function (value, type) {
                        return ServiceOutput.print(value, type);
                    };

                    //noinspection JSUnresolvedVariable
                    switch ($scope.field.component) {

                        case 'indexed':
                        case 'input[date-interval]':

                            if (!$scope.record) {
                                $scope.record = {};
                            }
                            if (!$scope.record[$scope.field.key]) {
                                $scope.record[$scope.field.key] = {};
                            }
                            break;

                        case 'select[local]':

                            if (!$scope.field.settings) {
                                $scope.field.settings = {};
                            }
                            if (!$scope.field.settings.id) {
                                $scope.field.settings.id = 'id';
                            }
                            if (!$scope.field.settings.text) {
                                $scope.field.settings.text = 'text';
                            }
                            break;

                        case 'select[remote]':

                            var remote = {
                                /**
                                 * ng-model
                                 */
                                text: "",
                                /**
                                 * @var {boolean}
                                 */
                                visible: false,
                                /**
                                 * store to original selected text
                                 */
                                original: "",
                                /**
                                 * temp storage to search term
                                 */
                                searching: "",
                                /**
                                 * @var {int}
                                 */
                                position: -1,
                                /**
                                 * @var {object}
                                 */
                                relationship: $scope.field.relationship,
                                /**
                                 * @var {int}
                                 */
                                take: 10,
                                /**
                                 * @var {int}
                                 */
                                skip: 0,
                                /**
                                 * @var {int}
                                 */
                                total: 0,
                                /**
                                 * @var {Array}
                                 */
                                collection: [],
                                /**
                                 * @var {boolean}
                                 */
                                canHide: true,
                                /**
                                 *
                                 * @param value
                                 */
                                init: function (value) {

                                    if (value) {

                                        var
                                            show = '',
                                            search = [];

                                        search.push([remote.relationship.value, value]);

                                        remote._search(search, function (response) {

                                            var collection = response.rows,
                                                selected = {};

                                            if (angular.isArray(collection)) {

                                                if (collection.length) {

                                                    selected = collection[0];

                                                    show = selected[remote.relationship.show];

                                                    remote.original = show;
                                                    remote.text = show;
                                                }
                                            }
                                        });
                                    }
                                },
                                /**
                                 *
                                 * @param searchable
                                 * @param callback
                                 * @private
                                 */
                                _search: function (searchable, callback) {

                                    var
                                        relationship = remote.relationship;

                                    if (relationship.module && relationship.entity && relationship.operation) {

                                        var
                                            filters = [],
                                            sorters = null,
                                            aggregators = null,
                                            limiters = [remote.skip, remote.take],
                                            fields = null;

                                        if (typeof searchable === 'string') {

                                            remote.searching = searchable;

                                            if (relationship && relationship.show && searchable) {
                                                filters.push([relationship.show, searchable, '*=']);
                                            }
                                        } else if (searchable) {

                                            filters = searchable;
                                        }


                                        ServiceApi.request(relationship.module, relationship.entity, relationship.operation,
                                            [filters, sorters, aggregators, limiters, fields],
                                            function (ss, ok, status, data) {

                                                if (ok) {

                                                    remote.collection = data.rows;
                                                    remote.total = data.total;
                                                    remote.last = Math.ceil(data.total / remote.take);

                                                    if (angular.isFunction(callback)) {
                                                        callback.call(this, data);
                                                    }
                                                }
                                            }
                                        );

                                    }

                                },
                                /**
                                 *
                                 */
                                keyup: function (_event) {

                                    switch (_event.keyCode) {

                                        /* ESC */
                                        case 27:
                                            remote.hide();
                                            break;

                                        /* ARROW UP */
                                        case 38:
                                            remote.visible = true;
                                            remote.arrow(-1);
                                            break;

                                        /* ARROW DOWN */
                                        case 40:
                                            remote.visible = true;
                                            remote.arrow(1);
                                            break;

                                        /* ARROW RIGHT */
                                        case 39:
                                            if (remote.visible && remote.position > -1) {
                                                remote.next();
                                            }
                                            break;

                                        /* ARROW LEFT */
                                        case 37:
                                            if (remote.visible && remote.position > -1) {
                                                remote.previous();
                                            }
                                            break;

                                        /* ENTER */
                                        case 13:
                                            if (angular.isArray(remote.collection)) {
                                                remote.collection.forEach(function (row) {
                                                    if (row.selected) {
                                                        remote.select(row);
                                                    }
                                                });
                                            }
                                            break;

                                        default:
                                            remote.skip = 0;
                                            remote.searching = remote.text;
                                            remote._search(remote.searching);
                                            break;
                                    }
                                },
                                /**
                                 *
                                 */
                                previous: function () {

                                    var search = false,
                                        skip = remote.skip - remote.take;

                                    if (skip < 0) {
                                        skip = 0;
                                    } else {
                                        search = true;
                                    }

                                    remote.skip = skip;

                                    if (search) {
                                        remote._search(remote.searching, function () {
                                            remote.position = -1;
                                            remote.arrow(1);
                                        });
                                    }

                                    remote.canHide = false;

                                    $timeout(function () {
                                        remote.canHide = true;
                                    }, 250);
                                },
                                /**
                                 *
                                 */
                                next: function () {

                                    var search = false,
                                        skip = ((remote.skip / remote.take) + 1) * remote.take,
                                        last = (remote.last - 1) * remote.take;

                                    console.log(remote.skip, remote.take, remote.last);

                                    if (skip > last) {
                                        skip = last;
                                    } else {
                                        search = true;
                                    }

                                    remote.skip = skip;

                                    if (search) {
                                        remote._search(remote.searching, function () {
                                            remote.position = -1;
                                            remote.arrow(1);
                                        });
                                    }

                                    remote.canHide = false;

                                    $timeout(function () {
                                        remote.canHide = true;
                                    }, 250);
                                },
                                /**
                                 *
                                 * @param factor
                                 */
                                arrow: function (factor) {

                                    var _arrow = function (arrow) {

                                        var index = (remote.position + arrow),
                                            min = 0,
                                            max = remote.collection.length - 1;

                                        if (index < min) {
                                            index = min;
                                        } else if (index > max) {
                                            index = max;
                                        }

                                        remote.position = index;

                                        //console.log(index, min, max);

                                        remote.collection.forEach(function (c) {
                                            c.selected = false;
                                        });

                                        if (remote.collection[index]) {
                                            remote.collection[index].selected = true;
                                        }
                                    };

                                    if (angular.isArray(remote.collection)) {

                                        if (remote.collection.length) {
                                            _arrow.call(this, factor);
                                        } else {
                                            remote._search(remote.searching, function () {
                                                _arrow.call(this, factor);
                                            });
                                        }
                                    }
                                },
                                /**
                                 *
                                 * @param selected
                                 */
                                select: function (selected) {

                                    remote.visible = false;

                                    var show = remote.relationship.show,
                                        value = remote.relationship.value;

                                    $scope.record[$scope.field.key] = selected[value];

                                    $scope.listener('change', $scope.items, $scope.field, $scope.record);

                                    remote.searching = "";
                                    remote.original = selected[show];
                                    remote.text = selected[show];
                                    remote.position = -1;

                                    remote.collection = [];
                                },
                                /**
                                 *
                                 */
                                focus: function (field) {

                                    remote.position = -1;
                                    if (!field.readonly && !field.disabled) {

                                        remote.visible = true;

                                        if (remote.searching) {

                                            remote.text = remote.searching;
                                        } else {

                                            if (remote.collection && remote.collection.length <= 1) {
                                                remote._search("");
                                            }

                                            remote.original = remote.text;
                                        }
                                    }
                                },
                                /**
                                 *
                                 */
                                blur: function () {

                                    $timeout(function () {
                                        if (remote.canHide) {

                                            remote.hide();
                                        } else {

                                            remote.canHide = true;
                                        }
                                    }, 200);

                                    if (remote.searching) {
                                        remote.text = remote.original;
                                    }
                                },
                                /**
                                 *
                                 */
                                hide: function () {

                                    remote.position = -1;
                                    remote.visible = false;
                                },
                                /**
                                 *
                                 */
                                add: function (text) {
                                    alert(text);
                                    // call api,
                                    // save the item,
                                    // get the id and put in the model
                                },
                                /**
                                 *
                                 * @param row
                                 * @returns {*}
                                 */
                                item: function (row) {

                                    function preg_quote(str) {
                                        return (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
                                    }

                                    var show = row[remote.relationship.show],
                                        search = remote.searching;

                                    if (search) {
                                        show = show.replace(new RegExp("(" + preg_quote(search) + ")", 'gi'), "<i>$1</i>");
                                    }
                                    return show;
                                },
                                /**
                                 *
                                 * @param target
                                 */
                                prevent: function (target) {

                                    var e = angular.element(target)[0];

                                    if (remote.text) {

                                        if (e.createTextRange) {
                                            var range = e.createTextRange();
                                            range.move('character', remote.text.length);
                                            range.select();
                                        } else {
                                            if (e.setSelectionRange) {
                                                e.setSelectionRange(remote.text.length, remote.text.length);
                                            }
                                        }
                                    }
                                }
                            };

                            $scope.field.remote = remote;
                            break;
                    }

                }]
            /**
             *
             * @param scope
             * @param element
             * @param attrs
             */
            , link: function (scope, element, attrs) {

                scope.id = scope.field.key + '_' + App.uniqid();
                scope.error = 'Field is required';
                if (App.language) {
                    scope.error = App.lang('validation.error');
                }
                scope.hint = 'hint--top-right hint--rounded hint--bounce';

                var html = '';

                //noinspection JSUnresolvedVariable
                switch (scope.field.component) {

                    case 'input[text]':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}" \
                                    ng-class="{\'has-error\': form[field.key].$invalid}">\
                                    <label for="{{::id}}" data-hint="{{::field.title}}" class="{{::hint}}">\
                                        {{::field.label}} <i ng-show="field.required">*</i>\
                                        <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small> \
                                    </label>\
                                    <input type="text" ng-model="record[field.key]" ng-change="listener(\'change\', items, field, record)"\
                                        id="{{::id}}" name="{{::field.key}}" class="form-control"\
                                        placeholder="{{::field.placeholder}}" autocomplete="off"\
                                        ng-required="field.required" ng-readonly="field.readonly" ng-disabled="field.disabled">\
                                </div>\
                             </div>';
                        break;

                    case 'input[password]':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}" ng-class="{\'has-error\': form[field.key].$invalid}">\
                                    <label for="{{::id}}" data-hint="{{::field.title}}" class="{{::hint}}">\
                                        {{::field.label}} <i ng-show="field.required">*</i>\
                                        <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small> \
                                    </label>\
                                    <input type="password" ng-model="record[field.key]" ng-change="listener(\'change\', items, field, record)"\
                                        id="{{::id}}" name="{{::field.key}}" class="form-control"\
                                        placeholder="{{::field.placeholder}}" autocomplete="off"\
                                        ng-required="field.required" ng-readonly="field.readonly" ng-disabled="field.disabled">\
                                </div>\
                             </div>';
                        break;

                    case 'input[number]':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}" ng-class="{\'has-error\': form[field.key].$invalid}">\
                                    <label for="{{::id}}" data-hint="{{::field.title}}" class="{{::hint}}">\
                                        {{::field.label}} <i ng-show="field.required">*</i>\
                                        <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small> \
                                    </label>\
                                    <input type="number" ng-model="record[field.key]" ng-change="listener(\'change\', items, field, record)"\
                                        id="{{::id}}" name="{{::field.key}}" class="form-control"\
                                        placeholder="{{::field.placeholder}}" autocomplete="off"\
                                        ng-required="field.required" ng-readonly="field.readonly" ng-disabled="field.disabled">\
                                </div>\
                             </div>';
                        break;

                    case 'input[radio]':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}" ng-class="{\'has-error\': form[field.key].$invalid}">\
                                    <label for="{{::id}}" data-hint="{{::field.title}}" class="{{::hint}}">\
                                        {{::field.label}} <i ng-show="field.required">*</i>\
                                        <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small> \
                                    </label>\
                                    <div class="radio">\
                                        <label class="radio radio-inline m-r-10" ng-repeat="_option in field.options">\
                                            <input type="radio" ng-model="record[field.key]" ng-change="listener(\'change\', items, field, record)"\
                                            id="{{::id}}" name="{{::field.key}}" value="{{::_option.id}}"\
                                            ng-required="field.required" ng-readonly="field.readonly" ng-disabled="field.disabled">\
                                            <i class="input-helper"></i> {{::_option.text}}\
                                        </label>\
                                    </div>\
                                </div>\
                             </div>';
                        break;

                    case 'input[checkbox]':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}" \
                                ng-class="{\'has-error\': form[field.key].$invalid}">\
                                    <label for="{{::id}}" data-hint="{{::field.title}}" class="{{::hint}}">\
                                        {{::field.label}} <i ng-show="field.required">*</i>\
                                        <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small> \
                                    </label>\
                                    <div class="checkbox" ng-class="{\'disabled\': (field.disabled || field.readonly)}">\
                                        <label>\
                                            <input type="checkbox" ng-model="record[field.key]" ng-change="listener(\'change\', items, field, record)"\
                                                ng-true-value="true" ng-false-value="false"\
                                                id="{{::id}}" name="{{::field.key}}"\
                                                ng-required="field.required" ng-readonly="field.readonly" \
                                                ng-disabled="(field.disabled || field.readonly)">\
                                                <i class="input-helper"></i> {{::field.placeholder}}\
                                        </label>\
                                    </div>\
                                </div>\
                             </div>';
                        break;

                    case 'input[color]':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}" ng-class="{\'has-error\': form[field.key].$invalid}">\
                                    <label for="{{::id}}" data-hint="{{::field.title}}" class="{{::hint}}">\
                                        {{::field.label}} <i ng-show="field.required">*</i>\
                                        <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small> \
                                    </label>\
                                    <input type="text" ng-model="record[field.key]" ng-change="listener(\'change\', items, field, record)"\
                                        id="{{::id}}" name="{{::field.key}}"\
                                        placeholder="{{::field.placeholder}}"\
                                        ng-required="field.required" ng-readonly="field.readonly" ng-disabled="field.disabled">\
                                </div>\
                             </div>';
                        break;

                    case 'textarea':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}" \
                                ng-class="{\'has-error\': form[field.key].$invalid}">\
                                    <label for="{{::id}}" data-hint="{{::field.title}}" class="{{::hint}}">\
                                        {{::field.label}} <i ng-show="field.required">*</i>\
                                        <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small> \
                                    </label>\
                                    <textarea  ng-model="record[field.key]" ng-change="listener(\'change\', items, field, record)"\
                                        id="{{::id}}" name="{{::field.key}}" class="form-control"\
                                        placeholder="{{::field.placeholder}}" autocomplete="off"  wrap="off" cols="405" rows="5"\
                                        ng-required="field.required" ng-readonly="field.readonly" ng-disabled="field.disabled"></textarea>\
                                </div>\
                             </div>';
                        break;

                    case 'list':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group"\
                                    ng-class="{\'has-error\': form[field.key].$invalid}">\
                                    <label for="{{::id}}" data-hint="{{::field.title}}" class="{{::hint}}">\
                                        {{::field.label}} <i ng-show="field.required">*</i>\
                                        <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small>\
                                    </label>\
                                    <div class="col-sm-12 no-padding" ng-repeat="_option in record[field.key]">\
                                        <div class="col-sm-1 no-padding">\
                                            <button class="btn btn-danger" ng-click="list.remove(field.key, _option)">\
                                                <i class="zmdi zmdi-delete"></i>\
                                            </button>\
                                        </div>\
                                        <div class="col-sm-6 no-padding" style="padding: 0 10px 0 0 !important;">\
                                            <div class="form-group form-group--simple {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}">\
                                                <input type="text" ng-model="_option.id" class="form-control" \
                                                    placeholder="Valor usado">\
                                            </div>\
                                        </div>\
                                        <div class="col-sm-5 no-padding" style="padding: 0 10px 0 0 !important;">\
                                            <div class="form-group form-group--simple {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}">\
                                                <input type="text" ng-model="_option.text" class="form-control"\
                                                    placeholder="Valor exibido">\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <div class="col-sm-12 no-padding">\
                                        <button class="btn btn-success" ng-click="list.add(field.key)">\
                                            <i class="zmdi zmdi-plus"></i>\
                                        </button>\
                                    </div>\
                                </div>\
                             </div>';
                        break;

                    case 'indexed':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}" ng-class="{\'has-error\': form[field.key].$invalid}">\
                                    <label for="{{::id}}" data-hint="{{::field.title}}" class="{{::hint}}">\
                                        {{::field.label}} <i ng-show="field.required">*</i>\
                                        <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small>\
                                    </label>\
                                    <div class="col-sm-12" style="padding: 0;">\
                                        <div class="{{::(_option.class ? _option.class : \'col-sm-12\')}} no-padding" \
                                            style="padding: 0 5px 0 5px !important;" \
                                            ng-repeat="_option in field.options">\
                                            <div class="form-group form-group--simple {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}">\
                                                <input type="text" ng-model="record[field.key][_option.id]" class="form-control" \
                                                    placeholder="{{::_option.text}}">\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                             </div>';
                        break;

                    case 'select[local]':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}" ng-class="{\'has-error\': form[field.key].$invalid}">\
                                    <label for="{{::id}}" data-hint="{{::field.title}}" class="{{::hint}}">\
                                        {{::field.label}} <i ng-show="field.required">*</i>\
                                        <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small>\
                                    </label>\
                                    <select chosen allow-single-deselect="true"\
                                        id="{{::id}}" name="{{::field.key}}" class="form-control"\
                                        ng-model="record[field.key]" ng-change="listener(\'change\', items, field, record)"\
                                        ng-required="field.required" ng-disabled="(field.readonly || field.disabled)"\
                                        ng-options="_option[field.settings.id] as _option[field.settings.text] for _option in field.options">\
                                            <option value=""></option>\
                                        </select>\
                                </div>\
                             </div>';
                        break;

                    case 'select[multiple]':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}" ng-class="{\'has-error\': form[field.key].$invalid}">\
                                    <label for="{{::id}}">{{::field.label}} <i ng-show="field.required">*</i>\
                                        <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small> \
                                    </label>\
                                    <select chosen multiple \
                                        id="{{::id}}" name="{{::field.key}}" class="form-control"\
                                        ng-model="record[field.key]" ng-change="listener(\'change\', items, field, record)"\
                                        ng-required="field.required" ng-readonly="field.readonly" ng-disabled="field.disabled"\
                                        ng-options="_option.id as _option.text for _option in field.options">\
                                            <option value=""></option>\
                                        </select>\
                                </div>\
                             </div>';
                        break;

                    case 'select[remote]':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}"\
                                    ng-class="{\'has-error\': form[field.key].$invalid,\'fg-toggled\':field.remote.visible}">\
                                    <label for="{{::id}}">{{::field.label}} <i ng-show="field.required">*</i>\
                                        <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small> \
                                    </label>\
                                    <input type="text" class="form-control" id="{{::id}}" ng-readonly="field.readonly" ng-disabled="field.disabled" ng-required="field.required" ng-model="field.remote.text" ng-keyup="field.remote.keyup($event)" ng-blur="field.remote.blur(field)" ng-focus="field.remote.focus(field)" autocomplete="off">\
                                </div>\
                                <div class="autocomplete" ng-show="field.remote.visible"\
                                    ng-init="field.remote.init(record[field.key])"\
                                    ng-class="{\'--visible\':field.remote.visible}">\
                                    <div ng-repeat="_collection in field.remote.collection">\
                                        <a class="autocomplete-item" ng-click="field.remote.select(_collection)" \
                                            ng-class="{\'--selected\':_collection.selected}">\
                                            <span ng-bind-html="field.remote.item( _collection) | sanitize"></span>\
                                        </a>\
                                    </div>\
                                    <div class="autocomplete-item --footer">\
                                        <div class="autocomplete-not-found" \
                                            ng-show="!field.remote.collection.length && field.remote.searching">\
                                        {{ settings.notFound ? settings.notFound : \'\' }}\
                                        </div>\
                                        <div class="pull-left">\
                                            <div class="autocomplete-include-more"\
                                                ng-class="{\'--separated\':(!field.remote.collection.length && field.remote.searching)}">\
                                                <a href="javascript:void(0);" class="btn btn-default"\
                                                    ng-click="field.remote.add(field.remote.searching)"\
                                                    ng-disabled="!field.remote.searching">+</a>\
                                                <span ng-show="field.remote.searching">{{ field.remote.searching }}</span>\
                                            </div>\
                                        </div>\
                                        <div class="pull-right">\
                                            <a href="javascript:void(0);" class="btn btn-default"\
                                                ng-click="field.remote.previous()" \
                                                ng-disabled="((field.remote.skip / field.remote.take) + 1) === 1">&laquo;</a>\
                                                {{ ((field.remote.skip / field.remote.take) + 1) }} / {{ field.remote.last }}\
                                            <a href="javascript:void(0);" class="btn btn-default" ng-click="field.remote.next()" \
                                                ng-disabled="((field.remote.skip / field.remote.take) + 1) === field.remote.last">&raquo;\
                                            </a>\
                                        </div>\
                                    </div>\
                                </div>\
                                <input type="hidden" name="{{::field.key}}" ng-model="record[field.key]"\
                                        ng-disabled="true" ng-required="field.required">\
                            </div>';
                        break;

                    case 'html':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group"\
                                    <label>{{::field.label}}</label>\
                                    <div ng-bind-html="record[field.key]"></div>\
                                </div>\
                             </div>';
                        break;

                    case 'input[date]':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}" ng-class="{\'has-error\': form[field.key].$invalid}">\
                                    <label for="{{::id}}" data-hint="{{::field.title}}" class="{{::hint}}">\
                                        {{::field.label}} <i ng-show="field.required">*</i>\
                                        <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small> \
                                    </label>\
                                    <input type="date" ng-model="record[field.key]" ng-change="listener(\'change\', items, field, record)"\
                                        id="{{::id}}" name="{{::field.key}}" class="form-control"\
                                        placeholder="{{::field.placeholder}}" autocomplete="off"\
                                        ng-required="field.required" ng-readonly="field.readonly" ng-disabled="field.disabled">\
                                </div>\
                             </div>';
                        break;

                    case 'input[cpf]':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}" ng-class="{\'has-error\': form[field.key].$invalid}">\
                                    <label for="{{::id}}" data-hint="{{::field.title}}" class="{{::hint}}">\
                                        {{::field.label}} <i ng-show="field.required">*</i>\
                                        <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small> \
                                    </label>\
                                    <input type="text" ng-model="record[field.key]" ng-change="listener(\'change\', items, field, record)"\
                                        id="{{::id}}" name="{{::field.key}}" class="form-control"\
                                        placeholder="{{::field.placeholder}}" autocomplete="off"\
                                        ng-keyup="mask($event, record, field.key, \'cpf\')"\
                                        ng-required="field.required" ng-readonly="field.readonly" ng-disabled="field.disabled">\
                                </div>\
                             </div>';
                        break;

                    case 'input[cnpj]':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}" ng-class="{\'has-error\': form[field.key].$invalid}">\
                                    <label for="{{::id}}" data-hint="{{::field.title}}" class="{{::hint}}">\
                                        {{::field.label}} <i ng-show="field.required">*</i>\
                                        <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small> \
                                    </label>\
                                    <input type="text" ng-model="record[field.key]" ng-change="listener(\'change\', items, field, record)"\
                                        id="{{::id}}" name="{{::field.key}}" class="form-control"\
                                        placeholder="{{::field.placeholder}}" autocomplete="off"\
                                        ng-keyup="mask($event, record, field.key, \'cnpj\')"\
                                        ng-required="field.required" ng-readonly="field.readonly" ng-disabled="field.disabled">\
                                </div>\
                             </div>';
                        break;

                    case 'input[cep]':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}" \
                                    ng-class="{\'has-error\': form[field.key].$invalid}">\
                                    <label for="{{::id}}" data-hint="{{::field.title}}" class="{{::hint}}">\
                                        {{::field.label}} <i ng-show="field.required">*</i>\
                                        <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small> \
                                    </label>\
                                    <input type="text" ng-model="record[field.key]" ng-change="listener(\'change\', items, field, record)"\
                                        id="{{::id}}" name="{{::field.key}}" class="form-control"\
                                        placeholder="{{::field.placeholder}}" autocomplete="off"\
                                        ng-keyup="mask($event, record, field.key, \'cep\')"\
                                        ng-required="field.required" ng-readonly="field.readonly" ng-disabled="field.disabled">\
                                </div>\
                             </div>';
                        break;

                    case 'input[money]':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}" ng-class="{\'has-error\': form[field.key].$invalid}">\
                                    <label for="{{::id}}" data-hint="{{::field.title}}" class="{{::hint}}">\
                                        {{::field.label}} <i ng-show="field.required">*</i>\
                                        <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small> \
                                    </label>\
                                    <input type="text" ng-model="record[field.key]" ng-change="listener(\'change\', items, field, record)"\
                                        id="{{::id}}" name="{{::field.key}}" class="form-control"\
                                        placeholder="{{::field.placeholder}}" autocomplete="off"\
                                        ng-keyup="money($event, record, field.key)"\
                                        ng-required="field.required" ng-readonly="field.readonly" ng-disabled="field.disabled">\
                                </div>\
                             </div>';
                        break;

                    case 'input[file]':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group" ng-class="{\'has-error\': form[field.key].$invalid}">\
                                    <label for="{{::id}}" data-hint="{{::field.title}}" class="{{::hint}}">\
                                        {{::field.label}} <i ng-show="field.required">*</i>\
                                        <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small> \
                                    </label>\
                                    <div class="form-group {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}">\
                                        <div class="fileinput"\
                                             ng-class="{\'fileinput-exists\': record[field.key], \'fileinput-new\': !record[field.key]}"\
                                             data-provides="fileinput">\
                                                <span class="btn bgm-teal btn-file m-r-10" ng-hide="(field.disabled || field.readonly)">\
                                                    <span class="fileinput-new">Selecione o Arquivo</span>\
                                                    <span class="fileinput-exists">Alterar o Arquivo</span>\
                                                    <input type="file" onchange="angular.element(this).scope().\
                                                                                    file(this, angular.element(this).scope())">\
                                                </span>\
                                                <span class="fileinput-filename"></span>\
                                                <span class="fileinput-filename--angular">\
                                                    <a href="{{ output(record[field.key], field.component) }}" target="_blank"> Download </a>\
                                                </span>\
                                            <a href="javascript:void(0);" class="close fileinput-exists" \
                                                ng-hide="(field.disabled || field.readonly)" data-dismiss="fileinput">&times;</a>\
                                        </div>\
                                    </div>\
                                    <input type="hidden" name="{{::field.key}}" id="{{::id}}" ng-model="record[field.key]" ng-required="field.required">\
                                </div>\
                            </div>';
                        break;

                    case 'input[date-interval]':
                        html =
                            '<div ng-class="field.wrapper" ng-hide="field.hidden">\
                                <div class="form-group {{::(!field.disabled && !field.readonly ? \'fg-line\' : \'\')}}" ng-class="{\'has-error\': form[field.key].$invalid}">\
                                    <div class="col-sm-12" style="padding: 0">\
                                        <label data-hint="{{::field.title}}" class="{{::hint}}">\
                                            {{::field.label}} <i ng-show="field.required">*</i>\
                                            <small class="help-block" ng-show="form[field.key].$invalid">{{::error}}</small> \
                                        </label>\
                                    </div>\
                                    <div class="col-sm-6" style="padding: 0 5px 0 0">\
                                        <input type="date" class="form-control" autocomplete="off"\
                                            name="{{::field.key}}" placeholder="{{::field.placeholder}}"\
                                            ng-model="record[field.key].start" ng-change="listener(\'change\', items, field, record)"\
                                            ng-required="field.required" ng-readonly="field.readonly" ng-disabled="field.disabled">\
                                    </div>\
                                    <div class="col-sm-6" style="padding: 0 0 0 5px">\
                                        <input type="date" class="form-control" autocomplete="off"\
                                            name="{{::field.key}}" placeholder="{{::field.placeholder}}"\
                                            ng-model="record[field.key].end" ng-change="listener(\'change\', items, field, record)"\
                                            ng-required="field.required" ng-readonly="field.readonly" ng-disabled="field.disabled">\
                                    </div>\
                                </div>\
                             </div>';
                        break;
                }

                if (!html) {

                    throw new Exception('app.directive: Component not found "' + scope.field.component + '" to field "' + scope.field.id + '"')
                } else {

                    $compile(html)(scope, function (_element, scope) {
                        element.append(_element);
                    });
                }
            }
        };
    });
