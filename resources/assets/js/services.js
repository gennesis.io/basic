App

// =========================================================================
// Header Messages and Notifications list Data
// =========================================================================

    .service('messageService', ['$resource', function ($resource) {
        this.getMessage = function (img, user, text) {
            var gmList = $resource("data/messages-notifications.json");

            return gmList.get({
                img: img,
                user: user,
                text: text
            });
        }
    }])


    // =========================================================================
    // Malihu Scroll - Custom Scroll bars
    // =========================================================================
    .service('scrollService', function () {
        var ss = {};
        ss.malihuScroll = function scrollBar(selector, theme, mousewheelaxis) {
            $(selector).mCustomScrollbar({
                theme: theme,
                scrollInertia: 100,
                axis: 'yx',
                mouseWheel: {
                    enable: true,
                    axis: mousewheelaxis,
                    preventDefault: true
                }
            });
        };

        return ss;
    })

    //==============================================
    // BOOTSTRAP GROWL
    //==============================================

    .service('ServiceGrowl', function () {
        var gs = {};
        gs.growl = function (message, type) {
            $.growl({
                message: message
            }, {
                type: type,
                allow_dismiss: true,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'bottom',
                    align: 'left'
                },
                delay: 10000,
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                },
                offset: {
                    x: 20,
                    y: 20
                }
            });
        };

        return gs;
    })

    //==============================================
    // CUSTOMS CONTROLLERS
    //==============================================

    .service('ControllerService', ['$route', '$q', '$rootScope', function ($route, $q, $rootScope) {

        return {
            /**
             * @var {object}
             */
            requested: {
                controller: true
            },
            /**
             *
             * @returns {*}
             */
            resolve: function () {
                var
                    module = $route.current.params.module,
                    entity = $route.current.params.entity;

                if (this.requested[module + '.' + entity]) {
                    return true;
                }
                this.requested[module + '.' + entity] = true;

                var deferred = $q.defer();
                var script = document.createElement('script');
                script.src = 'routes' + '/' + 'controller' + '/' + module + '/' + entity + '/' + 'load';

                script.onload = function () {
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                };
                document.body.appendChild(script);

                return deferred.promise;
            }
        };
    }]);
