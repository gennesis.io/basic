/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */

/**
 * ViewController
 */
App.controller('ViewController',
    ['$scope', '$routeParams', 'ServiceView', 'ServiceModel', 'ServiceListener',
        function ($scope, $routeParams, ServiceView, ServiceModel, ServiceListener) {

            $scope.events = {
                /**
                 * @var {object}
                 */
                list: {},
                /**
                 *
                 * @param listen
                 * @param key
                 * @param fn
                 */
                add: function (listen, key, fn) {
                    if (!$scope.events.list[listen]) {
                        $scope.events.list[listen] = {};
                    }
                    $scope.events.list[listen][key] = fn;
                },
                /**
                 *
                 * @param listen
                 * @param key
                 * @param items
                 * @param field
                 * @param record
                 */
                trigger: function (listen, key, items, field, record) {

                    var fn = null;

                    if ($scope.events.list[listen]) {
                        fn = $scope.events.list[listen][key];
                    }
                    if (fn) {
                        if (angular.isFunction(fn)) {
                            fn.call(this, items, field, record);
                        }
                    }
                }
            };

            var
                module = $routeParams.module,
                entity = $routeParams.entity,
                data = $routeParams.data.split('/');

            $scope.name = App.camelize(entity);

            ServiceModel.record = {};

            if (data.length) {

                var index = data[0];
                data.shift();

                var value = {};
                for (var i = 0; i < data.length; ++i)
                    value[i] = data[i];

                $scope.__id__ = App.id;
                $scope.code = App.code;
                $scope.debug = App.debug;

                $scope.module = module;
                $scope.entity = entity;
                $scope.index = index;


                $scope.view = new ServiceView($scope, module, entity, index, value);

                //$scope.view.page.reset();


                $scope.listener = function (listen, items, field, record) {

                    $scope.events.trigger(listen, field.key, items, field, record);
                };

                ServiceListener.reset(module + '.' + entity + '.' + index);

                ['save', 'create', 'update'].forEach(function (action) {

                    ServiceListener.register('after' + '.' + action, module + '.' + entity + '.' + index, function (record, data, primaries) {

                        if (angular.isArray(primaries) && (parseInt(data.save) > 0 || parseInt(data) > 0)) {
                            primaries.forEach(function (primary) {
                                if (!record[primary]) {
                                    if (parseInt(data.save) > 0) {
                                        record[primary] = parseInt(data.save);
                                    } else if (parseInt(data) > 0) {
                                        record[primary] = parseInt(data);
                                    }
                                }
                            });
                        }

                        var id = App.id;
                        if (!record[id]) {
                            record[id] = data.id;
                        }

                        if (data.relations) {
                            var relations = data.relations;
                            angular.forEach(relations, function (relation, key) {
                                //TODO: update primary of relationships
                            });
                        }
                    });
                });

                ServiceListener.register('after.add', module + '.' + entity + '.' + index, function (record) {

                    if (!record) {
                        record = {};
                    }
                    record[App.id] = App.uniqid();
                });

                ServiceListener.register('after.recover-record-set', module + '.' + entity + '.' + index, function (record, data) {

                    ServiceModel.recordSet.data = data.rows;
                    ServiceModel.recordSet.total = data.total;
                });
            }
        }]);