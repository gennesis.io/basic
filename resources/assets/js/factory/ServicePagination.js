/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */

App.factory('ServicePagination', function () {

    var ServicePagination = {
        /**
         * @var {int}
         */
        pages: 1,
        /**
         * @var {int}
         */
        total: 1,
        /**
         * @var {boolean}
         */
        invalid: false,
        /**
         * @var {object}
         */
        settings: {
            /**
             * @var {int}
             */
            current: 1,
            /**
             * @var {int}
             */
            skip: 10,
            /**
             * @var {Array}
             */
            search: []
        },
        /**
         *
         */
        reset: function () {
            ServicePagination.settings.current = 1;
            ServicePagination.settings.search = [];
            ServicePagination.search.text = '';
        },
        /**
         * @var {object}
         */
        search: {
            /**
             * @var {boolean}
             */
            visible: false,
            /**
             * @var {string}
             */
            text: '',
            /**
             *
             */
            hide: function () {
                ServicePagination.search.visible = false;
            },
            /**
             *
             */
            show: function () {
                ServicePagination.search.visible = true;
            },
            /**
             *
             */
            clear: function () {
                ServicePagination.search.text = '';
            }
        },
        /**
         *
         * @returns {boolean}
         */
        next: function () {
            return ServicePagination.settings.current == ServicePagination.pages;
        },
        /**
         *
         * @returns {boolean}
         */
        previous: function () {
            return ServicePagination.settings.current == 1;
        },
        /**
         *
         * @returns {boolean}
         */
        last: function () {
            return (
                ServicePagination.settings.current == ServicePagination.pages
                ||
                ServicePagination.settings.current == ServicePagination.pages - 1
            );
        },
        /**
         *
         * @returns {boolean}
         */
        first: function () {
            return (ServicePagination.settings.current == 1 || ServicePagination.settings.current == 2);
        },
        /**
         *
         * @param current
         */
        current: function (current) {
            if (current >= 1 && current <= ServicePagination.pages) {
                ServicePagination.settings.current = current;
                return true;
            }
            return false;
        },
        /**
         *
         * @param id
         */
        load: function (id) {

            var settings = localStorage.getItem(id + '.' + 'settings');
            if (settings) {
                ServicePagination.settings = JSON.parse(settings);
            }
        },
        /**
         *
         * @param id
         * @param total
         */
        update: function (id, total) {

            var pages = Math.ceil(total / ServicePagination.settings.skip);

            ServicePagination.total = total;
            ServicePagination.pages = pages;

            ServicePagination.invalid = (ServicePagination.settings.current > pages);

            localStorage.setItem(id + '.' + 'settings', JSON.stringify(ServicePagination.settings));
        },
        /**
         *
         * @returns {*[]}
         */
        limiters: function () {
            return [ServicePagination.settings.skip * (ServicePagination.settings.current - 1), ServicePagination.settings.skip];
        },
        /**
         *
         * @param items
         */
        filters: function (items) {

            var
                text = ServicePagination.search.text,
                search = null;

            if (text) {
                ServicePagination.settings.search = text;
                search = [];
                angular.forEach(items, function(item) {
                    search.push([item.name, text, '*=']);
                    search.push('or');
                });
                search.pop();
            }

            return search;
        }
    };

    return ServicePagination;
});