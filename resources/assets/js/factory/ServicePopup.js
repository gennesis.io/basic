/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */

App.factory('ServicePopup', ['ServiceGrowl', 'ServiceModel', '$timeout', function (ServiceGrowl, ServiceModel, $timeout) {

    var ServicePopup = {
        /**
         * @var {boolean}
         */
        visible: false,
        /**
         * @var {object}
         */
        data: {},
        /**
         *
         * @param index
         * @param record
         * @param items
         * @param events
         */
        open: function (index, record, items, events) {

            $timeout(function () {
                ServicePopup.visible = true;

                var data = {};

                if (record) {

                    data = ServiceModel.format.front(angular.copy(record), items);

                    if (events && events.list && events.list.change) {
                        for (var i in events.list.change) {
                            if (events.list.change.hasOwnProperty(i)) {
                                var event = events.list.change[i];
                                var field = null;
                                items.forEach(function (item) {
                                   if (item.key === i) {
                                       field = item;
                                   }
                                });
                                if (angular.isFunction(event)) {
                                    event.call(this, items, field, record);
                                }
                            }
                        }
                    }
                }

                if (!data[App.id]) {
                    data[App.id] = App.uniqid();
                }

                ServicePopup.data[index] = data;
            },1);
        },
        /**
         *
         */
        close: function () {
            ServicePopup.visible = false;
            ServicePopup.data = {};
        },
        /**
         *
         * @param index
         */
        save: function (index) {

            if (!(ServiceModel.record[index] instanceof Array)) {
                ServiceModel.record[index] = [];
            }

            var
                found = false,
                data = ServicePopup.data[index];

            angular.forEach(ServiceModel.record[index], function (_record, i) {
                if (data[App.id] === _record[App.id]) {
                    found = true;
                    ServiceModel.record[index][i] = data;
                }
            });

            if (!found) {
                ServiceModel.record[index].push(data);
            }

            ServiceModel.$scope.form.$setDirty();
            //ServiceGrowl.growl('Item modificado com sucesso', 'inverse');

            ServicePopup.close();
        },
        /**
         *
         * @param index
         * @param data
         */
        remove: function (index, data) {

            angular.forEach(ServiceModel.record[index], function (_record, i) {
                if (data[App.id] === _record[App.id]) {
                    if (ServiceModel.record[index].splice(i, 1)) {

                        ServiceModel.$scope.form.$setDirty();
                        //ServiceGrowl.growl('Item modificado com sucesso', 'inverse');
                    }
                }
            });
        },
        /**
         *
         */
        reset: function () {
            ServicePopup.visible = false;
            ServicePopup.data = {};
        }
    };

    return ServicePopup;
}]);