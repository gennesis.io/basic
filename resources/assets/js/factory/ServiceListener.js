/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */

App.factory('ServiceListener', ['ServiceModel', function (ServiceModel) {

    var ServiceListener = {
        /**
         *
         * @var {object}
         */
        list: {},
        /**
         *
         * @param id
         */
        reset: function (id) {

            for (var event in ServiceListener.list) {
                if (ServiceListener.list.hasOwnProperty(event)) {
                    for (var _id in ServiceListener.list[event]) {
                        if (_id === id) {
                            console.log('clear');
                            ServiceListener.list[event][id] = [];
                        }
                    }
                }
            }

        },
        /**
         *
         * @param event
         * @param id
         * @param callable
         */
        register: function (event, id, callable) {

            if (!ServiceListener.list[event]) {
                ServiceListener.list[event] = {};
            }
            if (!ServiceListener.list[event][id]) {
                ServiceListener.list[event][id] = [];
            }

            ServiceListener.list[event][id].push(callable);
        },
        /**
         *
         * @param event
         * @param id
         * @param record
         * @param data
         * @param notifications
         */
        trigger: function (event, id, record, data, notifications) {

            if (App.debug) {
                console.log(event, id);
            }
            var calls = ServiceListener.list[event] ? ServiceListener.list[event] : [];
            for (var i in calls) {
                if (calls.hasOwnProperty(i) && i === id) {
                    var callable = calls[i];
                    if (angular.isArray(callable)) {
                        ServiceListener._run(callable, record, data, notifications);
                        if (App.debug) {
                            console.log('trigger.called: "', event, '" on "', i, '"');
                        }
                    }
                }
            }
        },
        /**
         *
         * @param calls
         * @param record
         * @param data
         * @param notifications
         * @private
         */
        _run: function (calls, record, data, notifications) {

            calls.forEach(function (callable) {

                if (angular.isFunction(callable)) {

                    var primaries = ServiceModel.getPrimary();
                    callable.call(this, record, data, primaries, notifications);
                }
            });
        }
    };

    return ServiceListener;
}]);