<?php
/*
 -------------------------------------------------------------------
 | @project: php@sistemaconcursos.com.br
 | @package: Apocalipse\basic\presentation\data
 | @file: Report.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 05/06/16 23:58
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Basic\Presentation\Data;


use Apocalipse\Core\Dao\Modifier;
use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Domain\Definition\Collection;
use Apocalipse\Core\Model\Dynamo;

/**
 * Class Report
 * @package Apocalipse\Basic\Presentation\Data
 */
class Report extends Dynamo
{
    /**
     * @param Collection $collection
     * @param Record $fields
     * @param Modifier $modifier
     * @return array
     */
    protected function query(Collection $collection, Record $fields, Modifier $modifier)
    {
        return $this->getDao()->read($collection, $fields, $modifier, $this->debug);
    }
}