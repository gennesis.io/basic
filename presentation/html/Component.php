<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Basic\Presentation\Html
 | @file: Component.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 17/04/16 08:31
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Basic\Presentation\Html;


use Apocalipse\Core\Model\Type\Object;

/**
 * Class Component
 * @package Apocalipse\Basic\Presentation\Html
 */
class Component extends Object
{
    /**
     * @var string
     */
    public $tag = '';

    /**
     * @var string
     */
    public $name = '';

    /**
     * @var string
     */
    public $label = '';

    /**
     * @var string
     */
    public $required = '';

    /**
     * @var string
     */
    public $value = '';

    /**
     * Component constructor.
     * @param string $tag
     * @param string $name
     * @param string $label
     * @param string $required
     * @param array $properties
     * @param string $value
     */
    public function __construct($tag, $name, $label, $required, $properties = [], $value = null)
    {
        parent::__construct(iif($properties, []));

        $this->tag = $tag;
        $this->name = $name;
        $this->label = $label;
        $this->required = $required;
        $this->value = $value;

        $this->id = sif($properties, 'id', $name . '_' . uniqid());
    }

    /**
     *
     */
    public function render()
    {
        $required = $this->required ? '*' : '';

        if (is_array($this->grid)) {
            $rules = [];
            foreach ($this->grid as $key => $value) {
                $rules[] = 'col' . '-' . $key . '-' . $value;
            }
            $wrapper = implode(' ', $rules);
        } else {
            $wrapper = 'col-sm-' . $this->grid;
        }

        ?>
        <div class="<?php out($wrapper); ?>">
            <div class="form-group">
                <label for="<?php out($this->id); ?>"
                       class="control-label"><?php out($this->label); ?><?php out($required); ?></label>
                <?php
                switch ($this->tag) {

                    case 'input':

                        switch ($this->type) {

                            case 'checkbox':
                                foreach ($this->options as $value => $label) {
                                    //TODO: compare $value with string function comparing
                                    $checked = ($value == $this->value) ? 'checked' : '';
                                    ?>
                                    <label style="text-align: justify">
                                        <input type="checkbox" id="<?php out($this->id); ?>"
                                               name="<?php out($this->name); ?>"
                                               value="<?php out($value); ?>" <?php out($checked); ?>/>
                                        <?php out($label); ?>
                                    </label>
                                    <?php
                                }
                                break;

                            case 'radio':
                                ?>
                                <div class="radio radio-inline">
                                    <?php
                                    foreach ($this->options as $value => $label) {
                                        //TODO: compare $value with string function comparing
                                        $checked = $value == $this->value ? 'checked' : '';
                                        ?>
                                        <label style="text-align: justify">
                                            <input type="radio" id="<?php out($this->id); ?>"
                                                   name="<?php out($this->name); ?>"
                                                   value="<?php out($value); ?>" <?php out($checked); ?>/>
                                            <?php out($label); ?>
                                        </label>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <?php
                                break;

                            default:
                                ?>
                                <input type="<?php out($this->type); ?>" id="<?php out($this->id); ?>"
                                    <?php out($this->readonly ? 'readonly' : ''); ?>
                                       name="<?php out($this->name); ?>" placeholder="<?php out($this->placeholder); ?>"
                                       mask="<?php out($this->mask); ?>" value="<?php out($this->value); ?>"
                                       class="form-control">
                                <?php
                                break;
                        }
                        break;

                    case 'select':
                        ?>
                        <select id="<?php out($this->id); ?>" name="<?php out($this->name); ?>"
                                onchange="<?php out($this->change); ?>" class="form-control">
                            <option><?php out($this->empty); ?></option>
                            <?php
                            $id = 'id';
                            $text = 'text';
                            if (!empty($this->settings) && $this->settings) {
                                $id = sif($this->settings, 'id', $id);
                                $text = sif($this->settings, 'text', $text);

                            }
                            if (!empty($this->options) && is_array($this->options)) {
                                foreach ($this->options as $option) {
                                    $option = is_array($option) ? (object)$option : $option;

                                    $selected = $this->value === $option->$id ? 'selected' : '';
                                    ?>
                                    <option value="<?php out($option->$id); ?>" <?php out($selected); ?>>
                                        <?php out($option->$text); ?>
                                    </option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <?php
                        break;

                    default:
                        out('The tag "' . $this->tag . '" is NOT SUPPORTED');
                        break;
                }
                ?>
            </div>
        </div>
        <?php
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * @param string $required
     */
    public function setRequired($required)
    {
        $this->required = $required;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

}