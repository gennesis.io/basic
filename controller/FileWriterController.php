<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Basic\Controller
 | @file: FileReader.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 15/04/16 08:01
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Basic\Controller;


use Apocalipse\Core\Domain\Content\Container;
use Apocalipse\Core\Domain\Content\Data;
use Apocalipse\Core\Helper\File;
use Apocalipse\Core\Domain\Controller;
use Apocalipse\Core\Flow\Wrapper;

/**
 * Class FileWriterController
 * @package Apocalipse\Basic\Controller
 */
class FileWriterController extends Controller
{
    /**
     * @param $route
     * @param Data $data
     * @return mixed|string
     */
    public function render($route, Data $data)
    {
        $content = [];
        $type = '';
        $info = [];

        $files = $data->getFiles();

        foreach ($files as $name => $file) {

            $temp = $file['tmp_name'];

            $file = path(implode(DIRECTORY_SEPARATOR, $route), md5($name) . '.' . File::extension($file['name']));
            $filename = path($this->context, $file);

            if (File::write($filename, 'touch')) {

                $move = move_uploaded_file($temp, $filename);

                if ($move) {

                    $content[$name] = $file;
                } else {
                    Wrapper::err("Error on save file in '" . $filename . "'");
                }
            } else {
                Wrapper::err("Can not write file in '" . $filename . "'");
            }
        }

        return new Container($content, $type, $info);
    }

    /**
     * @return string
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * @param string $root
     */
    public function setRoot($root)
    {
        $this->root = $root;
    }

}