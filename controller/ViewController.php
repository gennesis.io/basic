<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Basic\Controller
 | @file: Template.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 15/04/16 08:42
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Basic\Controller;


use Apocalipse\Core\Domain\Content\Container;
use Apocalipse\Core\Domain\Content\Data;
use Apocalipse\Core\Domain\Content\Renderer;
use Apocalipse\Core\Domain\Content\Scope;
use Apocalipse\Core\Domain\Controller;
use Apocalipse\Core\Domain\Data\Record;

/**
 * Class TemplateController
 * @package Apocalipse\Basic\Controller
 */
class ViewController extends Controller
{
    /**
     * @var string
     */
    private $views;

    /**
     * @var string
     */
    private $suffix;

    /**
     * @var string
     */
    private $design;

    /**
     * @var string
     */
    private $fallback;

    /**
     * ViewController constructor.
     * @param Record $context
     * @param string $views
     * @param string $suffix
     * @param string $design
     */
    public function __construct(Record $context, $views, $suffix, $design = null)
    {
        parent::__construct($context);

        $this->views = $views;
        $this->suffix = $suffix;
        $this->design = $design;
    }

    /**
     * @param $route
     * @param $data
     * @return mixed|null
     */
    protected function template($route, $data)
    {
        return $this->context->template;
    }

    /**
     * @param Scope $scope
     * @return Scope
     */
    public function scope(Scope $scope)
    {
        return $scope;
    }

    /**
     * @param $route
     * @param Data $data
     * @return Container
     */
    public final function render($route, Data $data)
    {
        /** @var Record $page */

        $page = $this->context;

        $template = $this->template($route, $data);

        $scope = $this->scope(new Scope($page, $route, $data));

        $renderer = new Renderer($this->views, $this->suffix, $this->design, $scope);

        $content = $renderer->compile($template, $page->cache);

        return new Container($content, Container::TYPE_HTML);
    }

    /**
     * @return string
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @param string $views
     */
    public function setViews($views)
    {
        $this->views = $views;
    }

    /**
     * @return string
     */
    public function getSuffix()
    {
        return $this->suffix;
    }

    /**
     * @param string $suffix
     */
    public function setSuffix($suffix)
    {
        $this->suffix = $suffix;
    }

    /**
     * @return string
     */
    public function getDesign()
    {
        return $this->design;
    }

    /**
     * @param string $design
     */
    public function setDesign($design)
    {
        $this->design = $design;
    }

    /**
     * @return string
     */
    public function getFallback()
    {
        return $this->fallback;
    }

    /**
     * @param string $fallback
     */
    public function setFallback($fallback)
    {
        $this->fallback = $fallback;
    }

}