<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Project\Controller
 | @file: AppController.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 06/04/16 09:17
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Basic\Controller;


use Apocalipse\Core\Helper\File;
use Apocalipse\Core\Helper\Text;

/**
 * Class AppController
 * @package Apocalipse\Project\Controller
 */
class AppController extends ViewController
{
    /**
     * @param $route
     * @param $data
     * @return mixed|null
     */
    protected function template($route, $data)
    {
        $route = $this->route($route);

        $template = implode('/', $route);

        $directive = isset($route[1]) ? $route[1] : '';

        if (count($route) > 4) {

            $module = $route[2];
            $entity = $route[3];
            $operation = $route[4];

            switch ($directive) {

                case 'view':

                    $template = implode('/', ['layout', $operation]);

                    if (File::exists(path(true, 'src', 'view', $module, $entity, $operation . '.' . $this->getSuffix()))) {

                        $this->setViews(path(true, 'src', 'view'));

                        $template = implode('/', [$module, $entity, $operation]);
                    }
                    break;

                case 'controller':

                    $this->setSuffix('js');
                    $this->setViews(path(true, 'src', 'view'));

                    $template = implode('/', [$module, $entity, '@controller']);

                    $this->setFallback('App.register("' . dashesToCamelCase($entity, true) . 'Controller", function($scope) { });');
                    break;
            }
        }

        return $template;
    }

    /**
     * @param $route
     * @return array
     */
    private function route($route)
    {
        array_shift($route);

        if (!count($route)) {
            $route = ['index.html'];
        }

        $last = count($route) - 1;
        $route[$last] = Text::replaceLast($route[$last], '.html', '');

        return $route;
    }

}