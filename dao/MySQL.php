<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Basic\Dao
 | @file: MySQL.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 03/04/16 10:13
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Basic\Dao;


use Apocalipse\Core\Dao\Driver;
use Apocalipse\Core\Dao\Filter;
use Apocalipse\Core\Dao\Join;
use Apocalipse\Core\Dao\Modifier;
use Apocalipse\Core\Domain\Definition\Collection;
use Apocalipse\Core\Helper\Console;
use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Domain\Definition\Behaviour;
use Apocalipse\Core\Domain\Definition\Field;
use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Helper\Text;
use Apocalipse\Core\Model\Type\Warning;
use Exception;
use PDO;

/**
 * Class MySQL
 * @package Apocalipse\Basic\Dao
 */
class MySQL extends Driver
{
    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * MySQL constructor.
     * @param string $dsn
     * @param string $user
     * @param string $password
     * @param array $parameters
     */
    public function __construct($dsn, $user, $password, $parameters = [])
    {
        parent::__construct($dsn, $user, $password, $parameters);

        try {

            $this->pdo = new PDO($dsn, $user, $password, [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

        } catch (Exception $ex) {

            $this->error($ex->getMessage(), Wrapper::STATUS_ERROR, $ex->getFile(), $ex->getLine());
        }
    }

    /**
     * @param $sql
     * @param $parameters
     *
     * @return \PDOStatement
     */
    public function statement($sql, $parameters)
    {
        $statement = false;

        if ($this->pdo) {

            try {

                $statement = $this->pdo->prepare($sql);

                if (is_array($parameters)) {
                    foreach ($parameters as $i => $parameter) {
                        $bind = $i;
                        if (is_numeric($i)) {
                            $bind = $i + 1;
                        }
                        $statement->bindValue($bind, $parameter, PDO::PARAM_STR);
                    }
                }

            } catch (Exception $e) {

                $this->error($e->getMessage(), Wrapper::STATUS_ERROR, $e->getFile(), $e->getLine());
            }

        } else {

            $this->error('PDO not loaded');
        }

        return $statement;
    }

    /**
     * @param Collection $collection
     * @param Record $record
     * @param Modifier $modifier
     * @param bool $debug
     *
     * @return string
     */
    public function create(Collection $collection, Record $record, Modifier $modifier, $debug)
    {
        $create = null;

        $fields = [];
        $parameters = [];
        $values = [];

        foreach ($record as $key => $value) {
            $fields[] = $this->quote($key);
            $parameter = ':' . $key;
            $parameters[] = $parameter;
            $values[$parameter] = $value;
        }

        $command = [];
        $command[] = 'INSERT INTO';
        $command[] = $collection->getName();
        $command[] = '(' . implode(', ', $fields) . ')';
        $command[] = 'VALUES';
        $command[] = '(' . implode(', ', $parameters) . ')';

        $sql = implode(' ', $command);

        if ($debug) {
            Wrapper::info($sql);
        }

        if ($statement = $this->statement($sql, $values)) {

            $execute = $statement->execute();
            if ($execute) {

                $create = $this->pdo->lastInsertId();
            } else {

                $this->error("Query '" . $sql . "' could not be executed");
            }
        } else {

            $this->error("Query '" . $sql . "' could not be prepared");
        }

        return $create;
    }

    /**
     * @param Collection $collection
     * @param Record $record
     * @param Modifier $modifier
     * @param bool $debug
     *
     * @return array
     */
    public function read(Collection $collection, Record $record, Modifier $modifier, $debug)
    {

        $command = [];
        $command[] = 'SELECT';

        $fields = [];
        foreach ($record->all() as $key => $value) {
            $identifier = $this->quote($key);
            $field = $identifier . ' AS ' . $this->quote($key);
            if ($value) {
                $value = Text::indexOf($value, '(') === 0 ? $value : addslashes($value);
                $field = '(' . $value . ')' . ' AS ' . $this->quote($key);
            }
            $fields[] = $field;
        }
        $command[] = implode(', ', $fields);

        $command[] = 'FROM';
        $command[] = $this->quote($collection->getName());

        // JOIN (optional|default: none)
        $joins = $modifier->getJoins();
        if (is_array($joins) && count($joins)) {

            $command[] = $this->join($joins);
        }

        // WHERE (required)
        $command[] = 'WHERE';
        $command[] = $this->where($modifier->getFilters());

        // GROUP (optional|default: none)
        $aggregators = $modifier->getAggregators();
        if (is_array($aggregators) && count($aggregators)) {
            $group = [];
            foreach ($aggregators as $aggregator) {
                $group[] = implode(',', $aggregator->rules);
            }
            $command[] = 'GROUP BY ' . implode(',', $group);
        }

        // ORDER (optional|default: none)
        $sorters = $modifier->getSorters();
        if (is_array($sorters) && count($sorters)) {
            $order = [];
            foreach ($sorters as $sorter) {
                $order[] = implode(',', $sorter->rules);
            }
            $command[] = 'ORDER BY ' . implode(',', $order);
        }

        // LIMIT (optional| default: [0,10000])
        $limiters = $modifier->getLimiters();
        if (!is_array($limiters) || (is_array($limiters) && !count($limiters))) {
            $limiters = [0, 10000];
        }
        $command[] = 'LIMIT ' . implode(',', $limiters);

        $results = [];

        $sql = implode(' ', $command);

        if ($debug) {
            Wrapper::info($sql);
        }

        if ($statement = $this->statement($sql, [])) {

            $execute = $statement->execute();
            if ($execute) {

                $results = $statement->fetchAll(PDO::FETCH_ASSOC);
            } else {

                $this->error("Query '" . $sql . "' could not be executed");
            }
        } else {

            $this->error("Query '" . $sql . "' could not be prepared");
        }

        return $results;
    }

    /**
     * @param Collection $collection
     * @param Record $record
     * @param Modifier $modifier
     * @param bool $debug
     *
     * @return boolean
     */
    public function update(Collection $collection, Record $record, Modifier $modifier, $debug)
    {
        $update = null;

        $command = [];
        $command[] = 'UPDATE';
        $command[] = $this->quote($collection->getName());

        // JOIN (optional|default: none)
        $joins = $modifier->getJoins();
        if (is_array($joins) && count($joins)) {

            $command[] = $this->join($joins);
        }

        $command[] = 'SET';

        $parameters = [];
        $values = [];
        foreach ($record as $key => $value) {
            $parameter = ':' . $key;
            $parameters[] = $this->quote($key) . " = " . $parameter;
            $values[$parameter] = $value;
        }
        $command[] = implode(', ', $parameters);

        // WHERE (required)
        $command[] = 'WHERE';
        $command[] = $this->where($modifier->getFilters());

        $sql = implode(' ', $command);

        if ($debug) {
            Wrapper::info($sql);
        }

        if ($statement = $this->statement($sql, $values)) {

            $update = $statement->execute();

            if (!$update) {
                $this->error("Query '" . $sql . "' could not be executed");
            }
        } else {

            $this->error("Query '" . $sql . "' could not be prepared");
        }

        return $update;
    }

    /**
     * @param Collection $collection
     * @param Record $record
     * @param Modifier $modifier
     * @param bool $debug
     *
     * @return boolean
     */
    public function delete(Collection $collection, Record $record, Modifier $modifier, $debug)
    {
        $delete = null;

        $command = [];
        $command[] = 'DELETE FROM';
        $command[] = $this->quote($collection->getName());

        // JOIN (optional|default: none)
        $joins = $modifier->getJoins();
        if (is_array($joins) && count($joins)) {

            $command[] = 'USING';
            $command[] = $this->quote($collection->getName());

            $command[] = $this->join($joins);
        }

        // WHERE (required)
        $command[] = 'WHERE';
        $command[] = $this->where($modifier->getFilters());

        $sql = implode(' ', $command);

        if ($debug) {
            Wrapper::info($sql);
        }

        if ($statement = $this->statement($sql, [])) {

            $delete = $statement->execute();

            if (!$delete) {
                $this->error("Query '" . $sql . "' could not be executed");
            }
        } else {

            $this->error("Query '" . $sql . "' could not be prepared");
        }

        return $delete;
    }

    /**
     * @param $entity
     * @return string
     */
    private function quote($entity)
    {
        $identifier = $this->addQuote($entity);

        if (Text::indexOf($entity, '.') !== false) {

            $peaces = explode('.', $entity);
            $quoted = [];
            foreach ($peaces as $peace) {
                $quoted[] = $this->addQuote($peace);
            }
            $identifier = implode('.', $quoted);
        }

        return $identifier;
    }

    /**
     * @param $identifier
     * @return string
     */
    private function addQuote($identifier)
    {
        return "`" . $identifier . "`";
    }

    /**
     * @param array $joins
     * @return string
     */
    private function join($joins)
    {
        $clausules = [];
        /** @var Join $join */

        foreach ($joins as $join) {
            $on = [];
            foreach ($join->getOn() as $source => $target) {
                $on[] = $this->quote($source) . ' = ' . $this->quote($target);
            }
            $command = [];
            $command[] = $join->getType() . ' JOIN ';
            $command[] = $this->quote($join->getCollection()->getName());
            $command[] = 'ON (' . implode(' ', $on) . ')';

            $clausules[] = implode(' ', $command);
        }

        return implode(' ', $clausules);
    }

    /**
     * @param $filters
     * @return string
     */
    private function where($filters)
    {
        /** @var Filter $filter */
        $where = [];
        $where[] = '(';

        $command[] = implode(' ', $where);

        if (is_array($filters) && count($filters)) {

            foreach ($filters as $filter) {

                if ($filter instanceof Filter) {

                    $identifier = $filter->identifier;
                    if (!is_numeric($filter->identifier) && !is_array($filter->identifier)) {
                        $identifier = $this->quote($filter->identifier);
                    }
                    $value = $filter->value;
                    if (!is_array($value) && !is_object($value)) {
                        $value = addslashes($value);
                    }

                    switch ($filter->operator) {
                        case 'in':
                            $where[] = $identifier . " IN " . "('" . implode("', '", $value) . "')";
                            break;
                        case 'nin':
                            $where[] = $identifier . " NOT IN " . "('" . implode("', '", $value) . "')";
                            break;
                        case '*=':
                            $where[] = $identifier . " LIKE " . "'%" . $value . "%'";
                            break;
                        case 'bti':
                            $where[] = "'" . $value . "' BETWEEN " . $this->quote($identifier[0]) . " AND " . $this->quote($identifier[1]);
                            break;
                        case 'btv':
                            $where[] = "'" . $identifier . "' BETWEEN '" . $value[0] . "' AND '" . $value[1] . "'";
                            break;
                        default:
                            $where[] = $identifier . " " . $filter->operator . " (" . "'" . $value . "'" . ")";
                            break;
                    }
                } else if (strtoupper($filter) === Filter::LOGIC_CONECTOR_OR || strtoupper($filter) === Filter::LOGIC_CONECTOR_AND) {

                    $where[] = ')';
                    $where[] = ' ' . strtoupper($filter) . ' ';
                    $where[] = '(';
                }
            }

        } else {

            $where[] = 'FALSE';
        }

        $where[] = ')';

        return implode('', $where);
    }

    /**
     * @return bool
     */
    public function transactionBegin()
    {
        return $this->pdo->beginTransaction();
    }

    /**
     * @return bool
     */
    public function transactionCommit()
    {
        return $this->pdo->commit();
    }

    /**
     * @return bool
     */
    public function transactionRollBack()
    {
        return $this->pdo->rollBack();
    }
}